using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InfoPanelBehaviour : MonoBehaviour
{

    private GameController.Celestial infoTarget;

    [SerializeField]
    private GameObject Name;

    [SerializeField]
    private GameObject Mass;

    [SerializeField]
    private GameObject Radius;

    [SerializeField]
    private GameObject Orbit;

    [SerializeField]
    private GameObject Rotation;

    void Start()
    {
        // Name.GetComponent<TextMeshProUGUI>().SetText(GameController.celestials[GameController.Celestial.Sun].ptName);
    }

    // Update is called once per frame
    void Update()
    {
        // sun by default
        if(Name.GetComponent<TextMeshProUGUI>().text == "Exemplo")
        {
            Name.GetComponent<TextMeshProUGUI>().SetText(GameController.celestials[GameController.Celestial.Sun].ptName);
            Mass.GetComponent<TextMeshProUGUI>().SetText(GameController.celestials[GameController.Celestial.Sun].mass.ToString() + " x 10^24kg");
            Radius.GetComponent<TextMeshProUGUI>().SetText(GameController.celestials[GameController.Celestial.Sun].radius.ToString() + " km");
            Orbit.GetComponent<TextMeshProUGUI>().SetText("NA");
            Rotation.GetComponent<TextMeshProUGUI>().SetText("NA");
        }
    }

    public void ChangeTarget(GameController.Celestial newTarget)
    {
        infoTarget = newTarget;

        if(infoTarget == GameController.Celestial.Sun)
        {
            Name.GetComponent<TextMeshProUGUI>().SetText(GameController.celestials[GameController.Celestial.Sun].ptName);
            Mass.GetComponent<TextMeshProUGUI>().SetText(GameController.celestials[GameController.Celestial.Sun].mass.ToString() + " x 10^24kg");
            Radius.GetComponent<TextMeshProUGUI>().SetText(GameController.celestials[GameController.Celestial.Sun].radius.ToString() + " km");
            Orbit.GetComponent<TextMeshProUGUI>().SetText("NA");
            Rotation.GetComponent<TextMeshProUGUI>().SetText("NA");
        }
        else
        {
            Name.GetComponent<TextMeshProUGUI>().SetText(GameController.celestials[infoTarget].ptName);
            Mass.GetComponent<TextMeshProUGUI>().SetText(GameController.celestials[infoTarget].mass.ToString() + " x 10^24kg");
            Radius.GetComponent<TextMeshProUGUI>().SetText(GameController.celestials[infoTarget].radius.ToString() + " km");
            Orbit.GetComponent<TextMeshProUGUI>().SetText(GameController.celestials[infoTarget].orbitalPeriod.ToString() + " dias");
            Rotation.GetComponent<TextMeshProUGUI>().SetText(GameController.celestials[infoTarget].rotationPeriod.ToString() + " horas");
        }

    }

}
