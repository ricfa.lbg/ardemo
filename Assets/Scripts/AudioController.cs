using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System;


/// <summary>
/// singleton queue for playing audio....
/// anyway not really needed since i'm not swapping screens
/// https://www.youtube.com/watch?v=6OT43pvUyfY
/// </summary>
public class AudioController : MonoBehaviour
{

    public Sound[] songs;
    public static AudioController instance;

    private int numCurrentlyPlaying = 0;
    private Sound currentlyPlaying;
    private bool isPlaying = false;
    private bool isPaused = false;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);

        foreach(Sound s in songs)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
        }
    }

    public void Play(string name)
    {

        //Sound s = Array.Find(songs, sound => sound.name == name);
        for(int i = 0; i < songs.Length; i+=1)
        {
            if(songs[i].name == name)
            {
                numCurrentlyPlaying = i;
                currentlyPlaying = songs[i];
                break;
            }
        }
        currentlyPlaying.source.Play();
    }

    public void Play(int num)
    {
        numCurrentlyPlaying = num;
        currentlyPlaying = songs[num];
        
        //Sound s = songs[num];
        if(isPaused)
        {
            currentlyPlaying.source.UnPause();
            isPaused = false;
        }
        else
        {
            currentlyPlaying.source.Play();
        }
        isPlaying = true;
    }

    public void PlayNext()
    {
        if(numCurrentlyPlaying < songs.Length-1)
        {
            Stop();
            isPlaying = false;
            Play(numCurrentlyPlaying + 1);
        }
    }
    public void PlayPrev()
    {
        if(numCurrentlyPlaying>0)
        {
            Stop();
            isPlaying = false;
            Play(numCurrentlyPlaying - 1);
        }
    }
    public void Pause()
    {
        currentlyPlaying.source.Pause();
        isPaused = true;
    }

    public void Play()
    {
        Play(numCurrentlyPlaying);
    }

    private void Stop()
    {
        currentlyPlaying.source.Stop();
        isPlaying = false;
    }
}
